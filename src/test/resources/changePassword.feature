Feature: ChangePassword

  Background:
    Given the "http://todo.ly/" page opened

  @Regression
  Scenario Outline: Change Password Successfully
    When log in with the following credentials
      | email    | <email>    |
      | password | <password> |
    And hit on Setting button
    And fill in the old password and new password
      | oldPassword | <password>        |
      | newPassword | <passwordChanged> |
    And hit on ok button
    And hit on log out button
    Then log in with the following credentials
      | email    | <email>           |
      | password | <passwordChanged> |
    And the password should be updated
    Examples:
      | email                     | password | passwordChanged |
      | lisbeth.quisbert@test.com | 123456   | 123456changed   |


  # Because the password was changed, and in the second run the first scenario will fail
  @Regression
  Scenario Outline: Change Password to the old password
    When log in with the following credentials
      | email    | <email>    |
      | password | <password> |
    And hit on Setting button
    And fill in the old password and new password
      | oldPassword | <password>        |
      | newPassword | <passwordChanged> |
    And hit on ok button
    And hit on log out button
    Then log in with the following credentials
      | email    | <email>           |
      | password | <passwordChanged> |
    And the password should be updated
    Examples:
      | email                     | password      | passwordChanged |
      | lisbeth.quisbert@test.com | 123456changed | 123456          |

  @Regression
  Scenario: Change Password with an invalid old password
    When log in with the following credentials
      | email    | lisbeth.quisbert@test.com |
      | password | 123456                    |
    And hit on Setting button
    And fill in the old password and new password
      | oldPassword | thisisnotmypassword |
      | newPassword | 123456changed       |
    And hit on ok button
    And hit on log out button
    Then an alert of invalid password should appear

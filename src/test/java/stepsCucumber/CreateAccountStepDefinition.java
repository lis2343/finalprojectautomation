package stepsCucumber;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import pages.CreateAccountForm;
import pages.Dashboard;
import pages.MainPage;

import java.util.Map;

public class CreateAccountStepDefinition {
    String email;
    MainPage mainPage = new MainPage();
    Dashboard dashboard = new Dashboard();
    CreateAccountForm createAccountForm = new CreateAccountForm();

    @When("sending the following data to create an account")
    public void sendingTheFollowingDataToCreateAnAccount(Map<String, String> data) {
        createAccountForm.fullNameTextBox.writeText(data.get("fullname"));
        createAccountForm.passwordTextBox.writeText(data.get("password"));
        createAccountForm.termsAgreementCheckbox.click();
    }

    @Then("the new account should be created")
    public void theNewAccountShouldBeCreated() {
        Assertions.assertTrue(dashboard.logoutButton.isControlDisplayed(),
                "ERROR trying to create a new account");
    }

    @And("the create account form opened")
    public void theCreateAccountFormOpened() {
        mainPage.signUpButton.click();
    }

    @And("the sign up button is hit")
    public void theSignUpButtonIsHit() {
        createAccountForm.signUpButton.click();
    }

    @When("generate and send a random email")
    public void generateAndSendARandomEmail() {
        String domain = "gmail.com";
        String emailAddress = "";
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        while (emailAddress.length() < 5) {
            int character = (int) (Math.random() * 26);
            emailAddress += alphabet.substring(character, character + 1);
            emailAddress += Integer.valueOf((int) (Math.random() * 99)).toString();
            emailAddress += "@" + domain;
        }
        email = emailAddress;
        createAccountForm.emailTextBox.writeText(email);
    }

    @Then("an alert with an error message should appear")
    public void anAlertWithAnErrorMessageShouldAppear() {
        Assertions.assertTrue(mainPage.messageAlert.isControlDisplayed(),
                "ERROR, the alert did not appear");
    }
}

package stepsCucumber;

import io.cucumber.java.en.Given;
import session.Session;

public class BaseStepDefinition {

    @Given("the {string} page opened")
    public void thePageOpened(String url) {
        Session.getInstance().getBrowser().get(url);
    }
}

package stepsCucumber;

import controls.BrowserAlert;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.jupiter.api.Assertions;
import pages.ChangePasswordForm;
import pages.Dashboard;
import pages.LoginForm;
import pages.MainPage;

import java.util.Map;

public class ChangePasswordStepDefinition {
    MainPage mainPage = new MainPage();
    LoginForm loginForm = new LoginForm();
    Dashboard dashboard = new Dashboard();
    ChangePasswordForm changePasswordForm = new ChangePasswordForm();

    @And("log in with the following credentials")
    public void logInWithTheFollowingCredentials(Map<String, String> data) {
        mainPage.loginButton.click();
        loginForm.emailTextBox.writeText(data.get("email"));
        loginForm.passwordTextBox.writeText(data.get("password"));
        loginForm.loginButton.click();
    }

    @And("hit on Setting button")
    public void hitOnSettingButton() {
        dashboard.settingsButton.click();
    }

    @And("fill in the old password and new password")
    public void fillInTheOldPasswordAndNewPassword(Map<String, String> data) {
        changePasswordForm.oldPasswordTextBox.writeText(data.get("oldPassword"));
        changePasswordForm.newPasswordTextBox.writeText(data.get("newPassword"));
    }

    @And("the password should be updated")
    public void thePasswordShouldBeUpdated() {
        Assertions.assertTrue(dashboard.logoutButton.isControlDisplayed(),
                "ERROR trying to update the password");
    }

    @Then("an alert of invalid password should appear")
    public void anAlertOfInvalidPasswordShouldAppear() {
        Assertions.assertTrue(BrowserAlert.isAlertPresent(), "ERROR the alert of invalid password is not present");
    }

    @And("hit on ok button")
    public void hitOnOkButton() {
        changePasswordForm.okButton.click();
    }

    @And("hit on log out button")
    public void hitOnLogOutButton() {
        dashboard.logoutButton.click();
    }
}

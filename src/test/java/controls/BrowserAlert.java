package controls;

import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import session.Session;

public class BrowserAlert {
    public static boolean isAlertPresent()
    {
        try
        {
            WebDriver driver = Session.getInstance().getBrowser();
            driver.switchTo().alert();
            return true;
        }
        catch (NoAlertPresentException Ex)
        {
            return false;
        }
    }
}

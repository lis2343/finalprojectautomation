package browser;

public class FactoryBrowser {
    public static IBrowser make(Browser browserType){
        IBrowser browser;

        switch (browserType){
            case CHROME:
                browser = new Chrome();
                break;
            case FIREFOX:
                browser = new Firefox();
                break;
            case EDGE:
                browser = new Edge();
                break;
            default:
                browser = new Safari();
                break;
        }
        return browser;
    }
}

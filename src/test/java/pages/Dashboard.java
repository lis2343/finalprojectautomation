package pages;

import controls.Button;
import org.openqa.selenium.By;

public class Dashboard {
    public Button settingsButton = new Button(By.xpath("//div[@id='ctl00_HeaderTopControl1_PanelHeaderButtons']/a[@href='javascript:OpenSettingsDialog();']"));
    public Button logoutButton = new Button(By.id("ctl00_HeaderTopControl1_LinkButtonLogout"));
}

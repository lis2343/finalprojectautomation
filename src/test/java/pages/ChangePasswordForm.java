package pages;

import controls.Button;
import controls.TextBox;
import org.openqa.selenium.By;

public class ChangePasswordForm {
    public TextBox oldPasswordTextBox = new TextBox(By.id("TextPwOld"));
    public TextBox newPasswordTextBox = new TextBox(By.id("TextPwNew"));
    public Button okButton = new Button(By.xpath("//div[@class='ui-dialog-buttonset']/button/span[text()='Ok']"));
}

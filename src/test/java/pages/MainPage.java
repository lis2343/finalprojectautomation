package pages;

import controls.Button;
import controls.ErrorMessage;
import org.openqa.selenium.By;

public class MainPage {
    public Button signUpButton = new Button(By.xpath("//img[@src='/Images/design/pagesignup.png']"));
    public Button loginButton = new Button(By.xpath("//img[@src='/Images/design/pagelogin.png']"));
    public ErrorMessage messageAlert = new ErrorMessage(By.id("ErrorMessageText"));
}

Feature: CreateAccount

  Background:
    Given the "http://todo.ly/" page opened
    And the create account form opened

  @Regression
  Scenario: Create an account in Todo.ly successfully
    When generate and send a random email
    And sending the following data to create an account
      | fullname | final.project.user |
      | password | 123456             |
    And the sign up button is hit
    Then the new account should be created

  @Regression
  Scenario: Create an account in Todo.ly without email value
    When sending the following data to create an account
      | fullname | final.project.user |
      | password | 123456             |
    And the sign up button is hit
    Then an alert with an error message should appear